//Jackson Kwan 1938023
package LinearAlgebra;

public class Vector3d {
	private double x;
	private double y;
	private double z;
	
	public Vector3d(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getZ() {
		return z;
	}
	
	public double magnitude() {
		double m = Math.sqrt(Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2));
		return m;
	}
	
	public double dotProduct(Vector3d vec2) {
		double result = ((x*vec2.getX()) + (y*vec2.getY()) + (z*vec2.getZ()));
		return result;
	}
	
	public Vector3d add(Vector3d vec2) {
		double x2 = x + vec2.getX();
		double y2 = y + vec2.getY();
		double z2 = z + vec2.getZ();
		Vector3d vecResult = new Vector3d(x2,y2,z2);
		return vecResult;
	}
	
}
