package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {
	@Test
	public void getTest() {
		Vector3d testVec = new Vector3d(3,7,10);
		double getTester = testVec.getX();
		assertEquals(3, getTester);
	}
	
	@Test
	public void magntiudeTestWhole() {
		Vector3d testVec = new Vector3d(2,6,9);
		double magTest = testVec.magnitude();
		assertEquals(11, magTest);
	}
	
	@Test
	public void magnitudeTestDecimal() {
		Vector3d testVec = new Vector3d(1.1, 2.3, 10.32);
		double magTest = testVec.magnitude();
		assertEquals(10.63, magTest, 0.01);
	}
	
	@Test
	public void magnitudeTestNegative() {
		Vector3d testVec = new Vector3d(-2, -4.5, -10);
		double magTest = testVec.magnitude();
		assertEquals(11.15, magTest, 0.01);
	}
	
	@Test
	public void dotProductTestWhole() {
		Vector3d testVec = new Vector3d(1,4,9);
		Vector3d testVec2 = new Vector3d(3,7,10);
		double dotProd = testVec.dotProduct(testVec2);
		assertEquals(121, dotProd);
	}
	
	@Test
	public void dotProductTestDecimal() {
		Vector3d testVec = new Vector3d(3.2,5.3,9.7);
		Vector3d testVec2 = new Vector3d(1.1,7.23,10.907);
		double dotProd = testVec.dotProduct(testVec2);
		assertEquals(147.6369, dotProd);
	}
	
	@Test
	public void dotProductTestNegative() {
		Vector3d testVec = new Vector3d(-3,-5,-9);
		Vector3d testVec2 = new Vector3d(1,7,10);
		double dotProd = testVec.dotProduct(testVec2);
		assertEquals(-128, dotProd);
	}
	
	@Test
	public void addTestWhole() {
		Vector3d testVec = new Vector3d(1,4,9);
		Vector3d testVec2 = new Vector3d(3,7,10);
		Vector3d testVec3 = testVec.add(testVec2);
		assertEquals(4, testVec3.getX());
		assertEquals(11, testVec3.getY());
		assertEquals(19, testVec3.getZ());
	}
	
	@Test
	public void addTestDecimal() {
		Vector3d testVec = new Vector3d(1.7,4.9,9.1);
		Vector3d testVec2 = new Vector3d(3.2,7.3,10);
		Vector3d testVec3 = testVec.add(testVec2);
		assertEquals(4.9, testVec3.getX());
		assertEquals(12.2, testVec3.getY());
		assertEquals(19.1, testVec3.getZ());
	}
	
	@Test
	public void addTestNegative() {
		Vector3d testVec = new Vector3d(1,4,-9);
		Vector3d testVec2 = new Vector3d(-3,-7,10);
		Vector3d testVec3 = testVec.add(testVec2);
		assertEquals(-2, testVec3.getX());
		assertEquals(-3, testVec3.getY());
		assertEquals(1, testVec3.getZ());
	}

}
